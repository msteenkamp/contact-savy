export const CONTACTS = [
  {
    firstName: 'Ben',
    surname: 'Nevis',
    number: '+27 82 299 5656',
    email: 'ben@fourways.com',
    picture: 'contact5.jpg'
  },
  {
    firstName: 'Caitlyn',
    surname: 'Bowens',
    number: '+27 74 334 5879',
    email: 'caitbow@hotmail.co.za',
    picture: 'contact2.jpg'
  },
  {
    firstName: 'Marcel',
    surname: 'Steenkamp',
    number: '+27 83 453 5155',
    email: 'msteenkamp0@gmail.com',
    picture: ''
  },
  {
    firstName: 'Brian',
    surname: 'Clayton',
    number: '+27 84 455 7854',
    email: 'brian@gmail.com',
    picture: 'contact3.jpg'
  },
  {
    firstName: 'Casandra',
    surname: 'Harris',
    number: '+27 82 864 2546',
    email: 'charris@gmail.com',
    picture: 'contact1.jpg'
  },
  {
    firstName: 'Britney',
    surname: 'Klerk',
    number: '+27 73 254 1587',
    email: 'brit@yahoo.com',
    picture: 'contact4.jpg'
  }
];
